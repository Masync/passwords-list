import os 
class Data:
    def __init__ (self, _title: str, _desc: str,_usr: str,  _pw: str):
        self._title = _title
        self._desc = _desc
        self._usr = _usr
        self._pw =  _pw  

class DataBook:
    def __init__ (self):
        self._book = []

    def add(self, _title: str, _desc: str,_usr: str,  _pw: str):
        os.system('cls')
        book_add = Data(_title, _desc,_usr , _pw)
        self._book.append(book_add)
    
    def showAll(self):
        os.system('cls')
        for idx in self._book:
            self._print_data(idx)

    def drop(self, usr):
        os.system('cls')
        for idx, contact in enumerate(self._book):
            if contact._title.lower() == usr.lower():
                del self._book[idx]
                break

    def search(self, usr):
        os.system('cls')
        for contact in self._book:
            if contact._title.lower() == usr.lower():
                self._print_data(contact)
                break

    def udate(self, usr):
         for i, contact in enumerate(self._book):
            if contact._title == usr:
                contact._title = str(input('New Title: '))
                contact._desc = str(input('New Description: '))
                contact._usr = str(input('New User: '))
                contact._pw = str(input('New Password : '))
                self._book[i] = contact
                break


    def _print_data(self, idx):
        print('--- * --- * --- * --- * --- * --- * --- * ---')
        print(f'Title: {idx._title}')
        print(f'Description: {idx._desc}')
        print(f'User: {idx._usr}')
        print(f'Password: {idx._pw}')
        print('--- * --- * --- * --- * --- * --- * --- * ---')

def run():
    dataBook = DataBook()
    while True:
        option_menu = input('''
            [a]dd 
            ------
            [u]pdate 
            ------
            [s]earch 
            ------
            [d]rop 
            ------
            [l]ist
            ------
            [e]xit
         ''')
        if option_menu == 'a':
           _title = input('-- Title: ')
           _desc = input('-- Description: ')
           _user = input('-- User: ')
           _pass = input('-- Password: ')
           
           dataBook.add(_title, _desc,_user, _pass )

        elif option_menu == 'u':
            update_user = input('User to update')
            dataBook.udate(update_user)
        elif option_menu == 's':
             search_user = input('User to search')
             dataBook.search(search_user)
        elif option_menu == 'd':
             drop_user = input('User to Drop')
             dataBook.drop(drop_user)
        elif option_menu == 'l':
            dataBook.showAll()
        elif option_menu == 'e':
            break




if __name__ == "__main__":

    print('''
▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄
█▀▄▄▀█░▄▄▀█░▄▄█░▄▄█░███░█▀▄▄▀█░▄▄▀█░▄▀███░▄▄▀█▀▄▄▀█▀▄▄▀
█░▀▀░█░▀▀░█▄▄▀█▄▄▀█▄▀░▀▄█░██░█░▀▀▄█░█░███░▀▀░█░▀▀░█░▀▀░
█░████▄██▄█▄▄▄█▄▄▄██▄█▄███▄▄██▄█▄▄█▄▄████▄██▄█░████░███
▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
 ''')
    run()